Temperature Sensors software.

GUI code written by Andric Li.
Driver code written by Timothy Bodrov.

Internship for DRS Technologies, 2019-2020.


Notes:
- The official manual for the MccDaq recommends that we let it sit and warm up for at least 30 minutes before starting the temperature reading process.
- Make sure to run InstaCal and set up the DAQ configuration file before running our program. It will not work without it!
- We are using RTD thermosensors. Make sure to set this in InstaCal.
