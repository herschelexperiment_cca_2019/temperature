﻿using MccDaq;

namespace TemperatureSensors
{
    public class Driver
    {
        //this corresponds to the number of sensors
        private readonly int nChannels;

        //only one daq board is going to be used at a time...
        private readonly MccBoard daq = new MccBoard(0);

        public Driver(int nTemperatureSensors = 5)
        {
            //the number of temperature sensors corresponds to the number of channels on the MccDaq
            nChannels = nTemperatureSensors;
        }

        /**
         * Call this method to request new temperature readings from the MccDAQ
         */
        public float[] getNewTemperatureData()
        {
            float[] temperatureDataBuffer = new float[nChannels];

            ErrorInfo eCode = daq.TInScan(0, nChannels-1, TempScale.Fahrenheit, temperatureDataBuffer, ThermocoupleOptions.Filter);
            
            if(eCode.Value != 0) {
                //if an error occurs, display a description of it in a popup window
                System.Windows.Forms.MessageBox.Show(eCode.Message);
            }

            return temperatureDataBuffer;
        }
    }
}
