﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace TemperatureSensors
{
    public partial class Chart : Form
    {
        private int chartXValueCounter = 0;

        private readonly Driver driver = new Driver();

        public Chart()
        {
            InitializeComponent();
            ChartLoad();
        }

        /*
         * Draws the chart from the data variables
         */
        void ChartLoad()
        {
            //assign the chart in the designer to a variable
            var chart = chart1.ChartAreas[0];

            //clearing the axis labels
            chart.AxisX.LabelStyle.Format = "";
            chart.AxisY.LabelStyle.Format = "";
            chart.AxisX.LabelStyle.IsEndLabelVisible = true;

            chart.AxisX.Minimum = 0;
            chart.AxisY.Minimum = 75;
            chart.AxisY.Maximum = 78;

            chart.AxisX.Interval = 1;
            chart.AxisY.Interval = 1;

            chart.AxisX.Title = "Time (s)";
            chart.AxisY.Title = "Temperature (f)";

            chart1.Series[0].Name = "InfraRed";
            chart1.Series[0].Color = Color.Coral;

            chart1.Series.Add("Orange");
            chart1.Series[1].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            chart1.Series[1].Color = Color.Orange;

            chart1.Series.Add("Green");
            chart1.Series[2].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            chart1.Series[2].Color = Color.Green;

            chart1.Series.Add("Blue");
            chart1.Series[3].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            chart1.Series[3].Color = Color.Blue;

            chart1.Series.Add("Violet");
            chart1.Series[4].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            chart1.Series[4].Color = Color.DarkViolet;

            chart1.Series.Add("Empty");
            chart1.Series[5].IsVisibleInLegend = false;
            chart1.Series[5].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            chart1.Series[5].Points.AddXY(0, 100);
        }


        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (button1.Text == "Run")
            {
                timer1.Enabled = true;
                button1.Text = "Pause";
            }
            else
            {
                button1.Text = "Run";
                timer1.Enabled = false;
            }
        }

        private void Chart_Load(object sender, EventArgs e)
        {

        }

        private void chart1_Click(object sender, EventArgs e)
        {

        }
        //Timer Code
        private void timer1_Tick(object sender, EventArgs e)
        {
            var chart = chart1.ChartAreas[0];
            int removeThis = chartXValueCounter - 5;

            float[] driverData = driver.getNewTemperatureData();
            //we only keep track of the last 8 seconds of temperature readings

            //indexing our freshly received data array and drawing the data

            chart1.Series[0].Points.AddXY(chartXValueCounter, driverData[0]); //infrared
            thermometer1.TempValue = driverData[0];

            chart1.Series[1].Points.AddXY(chartXValueCounter, driverData[1]); //orange
            thermometer2.TempValue = driverData[1];

            chart1.Series[2].Points.AddXY(chartXValueCounter, driverData[2]); //green
            thermometer3.TempValue = driverData[2];

            chart1.Series[3].Points.AddXY(chartXValueCounter, driverData[3]); //blue
            thermometer4.TempValue = driverData[3];

            chart1.Series[4].Points.AddXY(chartXValueCounter, driverData[4]); //violet
            thermometer5.TempValue = driverData[4];

            //TODO: experiment to see if changing this changes the scrolling behavior
            if (chart1.Series[0].Points.Count >= 6)
            {
                chart.AxisX.Minimum++;

                if(chartXValueCounter == 11)
                {
                    
                }

                /*chart1.Series[0].Points.RemoveAt(removeThis);
                chart1.Series[1].Points.RemoveAt(removeThis);
                chart1.Series[2].Points.RemoveAt(removeThis);
                chart1.Series[3].Points.RemoveAt(removeThis);*/
                //chart1.Series[4].Points.RemoveAt(removeThis);
                //commenting out one of the series makes the code work

                //this clears the chart data so overflow will not happen
                if (chartXValueCounter >= 180)
                {
                    for(int i = 0; i < 4; i++)
                    {
                        chart1.Series[i].Points.Clear();
                    }
                    chart.AxisX.Minimum = 0;
                    chartXValueCounter = -1;
                }

            }
            chartXValueCounter++;
        }
    }
}

