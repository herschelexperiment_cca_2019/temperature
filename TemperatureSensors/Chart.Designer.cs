﻿namespace TemperatureSensors
{
    partial class Chart
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.thermometer1 = new BERGtools.Thermometer();
            this.thermometer2 = new BERGtools.Thermometer();
            this.thermometer3 = new BERGtools.Thermometer();
            this.thermometer4 = new BERGtools.Thermometer();
            this.thermometer5 = new BERGtools.Thermometer();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // chart1
            // 
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(66, 139);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chart1.Series.Add(series1);
            this.chart1.Size = new System.Drawing.Size(802, 267);
            this.chart1.TabIndex = 0;
            this.chart1.Text = "chart1";
            this.chart1.Click += new System.EventHandler(this.chart1_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::TemperatureSensors.Properties.Resources.DRS_Daylight_Solutions_Logo_Red_300_DPI;
            this.pictureBox1.Location = new System.Drawing.Point(66, 29);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(785, 68);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(776, 367);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Run";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // thermometer1
            // 
            this.thermometer1.BackColor = System.Drawing.SystemColors.Control;
            this.thermometer1.BackgroundColor = System.Drawing.Color.Empty;
            this.thermometer1.FontColor = System.Drawing.Color.Black;
            this.thermometer1.Location = new System.Drawing.Point(120, 412);
            this.thermometer1.Name = "thermometer1";
            this.thermometer1.ScaleColor = System.Drawing.Color.Black;
            this.thermometer1.Size = new System.Drawing.Size(67, 180);
            this.thermometer1.TabIndex = 3;
            this.thermometer1.TemperatureColor = System.Drawing.Color.Coral;
            this.thermometer1.TempValue = null;
            this.thermometer1.YMaximum = 78D;
            this.thermometer1.YMinimum = 75D;
            this.thermometer1.YTicks = 10;
            // 
            // thermometer2
            // 
            this.thermometer2.BackColor = System.Drawing.SystemColors.Control;
            this.thermometer2.BackgroundColor = System.Drawing.Color.Empty;
            this.thermometer2.FontColor = System.Drawing.Color.Black;
            this.thermometer2.Location = new System.Drawing.Point(284, 412);
            this.thermometer2.Name = "thermometer2";
            this.thermometer2.ScaleColor = System.Drawing.Color.Black;
            this.thermometer2.Size = new System.Drawing.Size(67, 180);
            this.thermometer2.TabIndex = 4;
            this.thermometer2.TemperatureColor = System.Drawing.Color.Orange;
            this.thermometer2.TempValue = null;
            this.thermometer2.YMaximum = 78D;
            this.thermometer2.YMinimum = 75D;
            this.thermometer2.YTicks = 10;
            // 
            // thermometer3
            // 
            this.thermometer3.BackColor = System.Drawing.SystemColors.Control;
            this.thermometer3.BackgroundColor = System.Drawing.Color.Empty;
            this.thermometer3.FontColor = System.Drawing.Color.Black;
            this.thermometer3.Location = new System.Drawing.Point(433, 412);
            this.thermometer3.Name = "thermometer3";
            this.thermometer3.ScaleColor = System.Drawing.Color.Black;
            this.thermometer3.Size = new System.Drawing.Size(67, 180);
            this.thermometer3.TabIndex = 5;
            this.thermometer3.TemperatureColor = System.Drawing.Color.Green;
            this.thermometer3.TempValue = null;
            this.thermometer3.YMaximum = 78D;
            this.thermometer3.YMinimum = 75D;
            this.thermometer3.YTicks = 10;
            // 
            // thermometer4
            // 
            this.thermometer4.BackColor = System.Drawing.SystemColors.Control;
            this.thermometer4.BackgroundColor = System.Drawing.Color.Empty;
            this.thermometer4.FontColor = System.Drawing.Color.Black;
            this.thermometer4.Location = new System.Drawing.Point(578, 412);
            this.thermometer4.Name = "thermometer4";
            this.thermometer4.ScaleColor = System.Drawing.Color.Black;
            this.thermometer4.Size = new System.Drawing.Size(67, 180);
            this.thermometer4.TabIndex = 6;
            this.thermometer4.TemperatureColor = System.Drawing.Color.Blue;
            this.thermometer4.TempValue = null;
            this.thermometer4.YMaximum = 78D;
            this.thermometer4.YMinimum = 75D;
            this.thermometer4.YTicks = 10;
            // 
            // thermometer5
            // 
            this.thermometer5.BackColor = System.Drawing.SystemColors.Control;
            this.thermometer5.BackgroundColor = System.Drawing.Color.Empty;
            this.thermometer5.FontColor = System.Drawing.Color.Black;
            this.thermometer5.Location = new System.Drawing.Point(724, 412);
            this.thermometer5.Name = "thermometer5";
            this.thermometer5.ScaleColor = System.Drawing.Color.Black;
            this.thermometer5.Size = new System.Drawing.Size(67, 180);
            this.thermometer5.TabIndex = 7;
            this.thermometer5.TemperatureColor = System.Drawing.Color.DarkViolet;
            this.thermometer5.TempValue = null;
            this.thermometer5.YMaximum = 78D;
            this.thermometer5.YMinimum = 75D;
            this.thermometer5.YTicks = 10;
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Chart
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(911, 604);
            this.Controls.Add(this.thermometer5);
            this.Controls.Add(this.thermometer4);
            this.Controls.Add(this.thermometer3);
            this.Controls.Add(this.thermometer2);
            this.Controls.Add(this.thermometer1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.chart1);
            this.MaximizeBox = false;
            this.Name = "Chart";
            this.Text = "Herschel Experiment";
            this.Load += new System.EventHandler(this.Chart_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button1;
        private BERGtools.Thermometer thermometer1;
        private BERGtools.Thermometer thermometer2;
        private BERGtools.Thermometer thermometer3;
        private BERGtools.Thermometer thermometer4;
        private BERGtools.Thermometer thermometer5;
        private System.Windows.Forms.Timer timer1;
    }
}

